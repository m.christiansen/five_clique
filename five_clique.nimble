# Package
version = "0.1.0"
author = "Marc Christiansen"
description  = "A solution to the problem of finding five English words with 25 distinct characters, using graph theory."
license = "AGPL-3.0-only"
srcDir = "."
bin = @["generate_graph", "five_clique"]

# Dependencies
requires "nim >= 1.6"  # older untested
requires "suru >= 0.3.1"

# Tasks
task getData, "Download the list of words":
    exec "wget --xattr --compression=auto https://github.com/dwyl/english-words/raw/master/words_alpha.txt"
