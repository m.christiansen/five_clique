# Five Clique
This is a port of https://gitlab.com/bpaassen/five_clique to Nim.

A solution to the problem of finding five English words with 25 distinct characters, using graph theory.

Copyright 2022 - Marc Christiansen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation as version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Quickstart guide
1. Download the `words_alpha.txt` file from https://github.com/dwyl/english-words (this is the same file that Parker used), for example by executing  `nimble getData`.
2. Run `nimble -d:release build`.
2. Run the `generate_graph` file. (should take at most a few minutes)
3. Run the `five_clique` file. (should take at most a few hours)

All five-word groups with distinct letters should then be in the file `cliques.csv`.

## Description (copied from the python version, with small formatting changes; that means 'the author' refers to Benjamin Paassen)

This solution is inspired by the wonderful [A problem squared podcast](https://aproblemsquared.libsyn.com/) by Hill and Parker. In episode 38, Hill and Parker are searching for five English words with distinct letters. Parker's proposed solution builds pairs of distinct words and then tries to merge these pairs to groups of five (henceforth named 'The Parker algorithm'). According to Parker, executing Parker's algorithm on a laptop took about a month. This appeared to the author as optimizable.

The solution proposed here represents the problem as a graph. In particular, we consider all 5-letter words (without repeated letters) of the English language as nodes in a graph, and we say that two words are neighbors if they share no letters. Finding 5 words with distinct letters now is equivalent to finding a [5-clique](https://en.wikipedia.org/wiki/Clique_(graph_theory)) in this graph, meaning a cluster of 5 words where each word is neighbor to each other word in the cluster.

How do we find a 5-clique, then? We start at some word i and build a clique from there. First, we consider all neighbors j. The third word k in the clique now needs to be neighbor to both i and j. Therefore, we consider only words k in the intersection of the neighbor sets of i and j. Next, we consider words l in the intersection of the neighbor sets of i, j, and k. Finally, any words r in the intersection of the neighbor sets of i, j, k, and l form a 5-clique {i, j, k, l, r}. To avoid repititions, we only consider solutions where i < j < k < l < r.

In the worst case, this scheme has a complexity of O(n^5), where n is the number of 5-letter words in the English language. This may seem infeasible. However, the size of the intersection rapidly declines the deeper we go into the clique. Therefore, we are finished in 21 minutes and 46 seconds, rather than a month, as in the Parker algorithm.

But Parker gave it a go. And that's something.


## TODO
- Benchmark  `smallIntSet * largeIntSet` vs. `largeIntSet * smallIntSet`
