# Copyright 2022 Marc Christiansen

import std/[intsets, memfiles, sequtils, strutils]
import pkg/suru

type
    Entry = object
        word: string
        neighbours: IntSet
    Cliques = seq[array[5, int]]
    Word = set['a' .. 'z']

proc buildGraph: seq[Entry] =
    # words_alpha.txt from https://github.com/dwyl/english-words
    var
        words: seq[Word]
        mf = memfiles.open("words_alpha.txt")
    for word in suru(lines(mf)):
        if word.len != 5:
            continue
        let char_set = block:
            var s: Word
            for c in word: incl s, c
            s
        if card(char_set) != 5:
            continue
        # compute the 'neighbours' for the new word, i.e. other words which
        # have entirely distinct letters
        for j in 0 ..< words.len:
            if card(char_set * words[j]) == 0:
                result[j].neighbours.incl words.len
        words.add char_set
        result.add Entry(word: word)
    close mf

proc saveCliques(cliques: Cliques; words: seq[Entry]) =
    let f = io.open("cliques.csv", fmWrite)
    for clique in suru(cliques):
        # get word representation of cliques and write to output
        let clique_words = clique.mapIt(words[it].word)
        # Explicitly write "\x0d\x0a" as line ending to stay compatible with the python version.
        f.write clique_words.join("\t"), "\x0d\x0a"
    close f

proc extendCliques(n_ijk: IntSet; indices: array[5, int]; words: seq[Entry]; cliques: var Cliques) =
    var indices = indices
    for l in n_ijk:
        # intersect with neighbours of l
        let n_ijkl = n_ijk.intersection words[l].neighbours
        # all remaining neighbours form a 5-clique with i, j, k, and l
        for r in n_ijkl:
            indices[3] = l
            indices[4] = r
            cliques.add indices

proc findCliques(words: seq[Entry]): Cliques =
    for i in suru(0 .. words.high):
        let n_i {.cursor.} = words[i].neighbours
        for j in n_i:
            # the remaining candidates are only the words in the intersection
            # of the neighbourhood sets of i and j
            let n_ij = n_i.intersection words[j].neighbours
            if card(n_ij) < 3:
                continue
            for k in n_ij:
                # intersect with neighbours of k
                let n_ijk = n_ij.intersection words[k].neighbours
                if card(n_ijk) < 2:
                    continue
                extendCliques(n_ijk, [i, j, k, -1, -1], words, result)

proc main =
    # Here, now, begins the daunting task of finding five-cliques in the graph we
    # prepared via 'generate_graph.nim'.

    echo "--- loading graph ---"
    let words = buildGraph()

    echo "--- start clique finding (THIS WILL TAKE LONG!) ---"

    # start clique finding
    let cliques = findCliques(words)

    echo "completed! Found $1 cliques" % [$cliques.len]

    echo "--- write to output ---"
    saveCliques(cliques, words)

when isMainModule:
    main()

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation as version 3 of the License.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
